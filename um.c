#include <stdio.h>
#include "vm.h"

int main(int argc, char **argv) {
  FILE *file = argc > 1 ? fopen(argv[1], "rb") : stdin;
  vm_t *vm = vm_ctor();
  fseek(file, 0, SEEK_END);
  long fsize = ftell(file);
  fseek(file, 0, SEEK_SET);
  fread(vm->segments[0]->m, fsize, 1, file);
  fclose(file);
  interp_program(vm);
  return 0;
}
