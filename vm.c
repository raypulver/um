#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <bitpack.h>
#include "vm.h"

void abort2() {
  exit(1);
}

void die(vm_t *vm, char *s) {
  vm_dtor(&vm);
  printf("Error: %s\n", s);
  exit(1);
}

void die_no_free(char *s) {
  printf("Error: %s\n", s);
  exit(1);
}

void segment_dtor(segment_t *);
segment_t *segment_ctor(vm_t *, size_t);

vm_t *vm_ctor() {
  size_t a = sizeof(um_ptr *)*VM_DEFAULT_SEGMENTS;
  vm_t *retval = malloc(sizeof(*retval));
  if (!retval) die_no_free("malloc");
  retval->segments = malloc(VM_DEFAULT_SEGMENTS*sizeof(segment_t *));
  if (!retval->segments) die_no_free("malloc");
  memset(retval->segments, 0, a);
  memset(retval->r, 0, sizeof(retval->r));
  retval->segments[0] = segment_ctor(retval, 0x10000000);
  retval->a = VM_DEFAULT_SEGMENTS;
  return retval;
}

void vm_dtor(vm_t **vm) {
  for (int i = 0; i < (*vm)->a; ++i) {
    if ((*vm)->segments[i]) segment_dtor((*vm)->segments[i]);
  }
  free((*vm)->segments);
  *vm = NULL;
}

segment_t *segment_ctor(vm_t *vm, size_t sz) {
  segment_t *retval = malloc(sizeof(*retval));
  if (!retval) die(vm, "malloc");
  retval->m = malloc(sizeof(um_word)*sz);
  if (!retval->m) die(vm, "malloc segment");
  memset(retval->m, 0, sizeof(um_word)*sz);
  retval->sz = sz;
  return retval;
}

void segment_dtor(segment_t *seg) {
  if (!seg) return;
  free(seg->m);
  free(seg);
}

um_ptr vm_alloc_segment(vm_t *vm, um_word sz, um_word i) {
  i = 0;
  for (; i < vm->a; ++i) {
    if (vm->segments[i]) continue;
    break;
  }
  while (i >= vm->a) {
    vm->segments = realloc(vm->segments, (sizeof(segment_t *) * (vm->a << 1)));
    memset(vm->segments + vm->a, 0, sizeof(segment_t *) * (vm->a << 1) - sizeof(segment_t *) * vm->a);
    if (!vm->segments) die(vm, "realloc");
    vm->a <<= 1;
  }
  vm->segments[i] = segment_ctor(vm, sz < 0x1000 ? 0x1000 : sz);
  return (um_ptr) i;
}

void vm_dealloc_segment(vm_t *vm, um_word i) {
  if (!i) return;
  if (vm->segments[i]) {
    segment_dtor(vm->segments[i]);
    vm->segments[i] = NULL;
  }
}

uint8_t is_le() {
  union {
    uint8_t chars[2];
    uint16_t word;
  } un;
  un.word = 0x1000;
  if (un.chars[1]) return 1;
  return 0;
}

um_word swizzle(um_word w) {
  union {
    char bytes[4];
    um_word word;
  } un;
  un.word = w;
  char tmp;
  tmp = un.bytes[3];
  un.bytes[3] = un.bytes[0];
  un.bytes[0] = tmp;
  tmp = un.bytes[2];
  un.bytes[2] = un.bytes[1];
  un.bytes[1] = tmp;
  return un.word;
}

void bkpt() {}
void interp_program(vm_t *vm) {
  uint8_t le = is_le(); 
  um_word instr, word;
  char input;
  um_word a, b, c;
  if (vm->pc >= vm->segments[0]->sz) abort2();
  while ((instr = Bitpack_getu((word = le ? swizzle(vm->segments[0]->m[vm->pc]) : vm->segments[0]->m[vm->pc]), 4, 28)) != 7) {
//    if (instr != 13) printf("%u %u %u %u %u %u %u\n", instr, Bitpack_getu(word, 3, 0), Bitpack_getu(word, 3, 3), Bitpack_getu(word, 3, 6), vm->r[Bitpack_getu(word, 3, 0)], vm->r[Bitpack_getu(word, 3, 3)], vm->r[Bitpack_getu(word, 3, 6)]);
 //   if (instr == 13) printf("%u %u %u\n", instr, Bitpack_getu(word, 3, 25), Bitpack_getu(word, 25, 0));
    switch (instr) {
      case 0:
        if (vm->r[Bitpack_getu(word, 3, 0)]) vm->r[Bitpack_getu(word, 3, 6)] = vm->r[Bitpack_getu(word, 3, 3)];
        break;
      case 1:
        b = vm->r[Bitpack_getu(word, 3, 3)];
        c = vm->r[Bitpack_getu(word, 3, 0)];
        if (!vm->segments[b]) abort2();
        if (c >= vm->segments[b]->sz) abort2();
        vm->r[Bitpack_getu(word, 3, 6)] = vm->segments[vm->r[b]]->m[c];
        break;
      case 2:
        a = vm->r[Bitpack_getu(word, 3, 6)];
        b = vm->r[Bitpack_getu(word, 3, 3)];
        if (!vm->segments[a]) abort2();
        if (b >= vm->segments[a]->sz) {
          printf("%u\n", vm->segments[a]->sz);
          abort2();
        }
        vm->segments[a]->m[b] = vm->r[Bitpack_getu(word, 3, 0)];
        break;
      case 3:
        vm->r[Bitpack_getu(word, 3, 6)] = vm->r[Bitpack_getu(word, 3, 3)] + vm->r[Bitpack_getu(word, 3, 0)];
        break;
      case 4:
        vm->r[Bitpack_getu(word, 3, 6)] = vm->r[Bitpack_getu(word, 3, 3)] * vm->r[Bitpack_getu(word, 3, 0)];
        break;
      case 5:
        c = vm->r[Bitpack_getu(word, 3, 0)];
        if (!c) abort2();
        vm->r[Bitpack_getu(word, 3, 6)] = vm->r[Bitpack_getu(word, 3, 3)] / c;
        break;
      case 6:
        vm->r[Bitpack_getu(word, 3, 6)] = ~(vm->r[Bitpack_getu(word, 3, 3)] & vm->r[Bitpack_getu(word, 3, 0)]);
        break;
      case 7:
        die(vm, "");
        break;
      case 8:
        vm->r[Bitpack_getu(word, 3, 3)] = (um_word) vm_alloc_segment(vm, vm->r[Bitpack_getu(word, 3, 0)], 0);
        break;
      case 9:
        c = Bitpack_getu(word, 3, 0);
        b = vm->r[c];
        if (!c || !vm->segments[b]) abort2();
        vm_dealloc_segment(vm, b);
        break;
      case 10:
        c = vm->r[Bitpack_getu(word, 3, 0)];
        if (c > 255) abort2();
        printf("%c", (char) c);
        break;
      case 11:
        scanf("%c", &input);
        vm->r[Bitpack_getu(word, 3, 0)] = input;
        if (input == EOF) vm->r[Bitpack_getu(word, 3, 0)] = 0xffffffff;
        break;
      case 12:
        b = vm->r[Bitpack_getu(word, 3, 3)];
        if (b) {
          if (!vm->segments[b]) abort2();
          if (vm->segments[b]->sz > vm->segments[0]->sz) vm->segments[0]->m = realloc(vm->segments[0]->m, vm->segments[b]->sz*sizeof(um_word));
          memcpy(vm->segments[0]->m, vm->segments[b]->m, vm->segments[b]->sz*sizeof(um_word)); 
          vm->segments[0]->sz = vm->segments[b]->sz;
        }
        vm->pc = vm->r[Bitpack_getu(word, 3, 0)] - 1;
        break;
      case 13:
        vm->r[Bitpack_getu(word, 3, 25)] = Bitpack_getu(word, 25, 0);
        break;
      default:
        abort2();
        break;
    }
    vm->pc++;
    if (vm->pc >= vm->segments[0]->sz) abort2();
  }
}
