#include <stdint.h>
#include "types.h"

#define VM_DEFAULT_SEGMENTS 0x100
#define VM_SEGMENT_SIZE 0x10000


typedef struct _segment_t {
  um_word *m;
  um_word sz;
} segment_t;

typedef struct _vm_t {
  um_word r[8];
  um_ptr pc;
  segment_t **segments;
  um_word a;
} vm_t;

vm_t *vm_ctor();
void vm_dtor(vm_t **);
um_ptr vm_alloc_segment(vm_t *, um_word, um_word);
void vm_dealloc_segment(vm_t *, um_word);
void interp_program(vm_t *vm);
